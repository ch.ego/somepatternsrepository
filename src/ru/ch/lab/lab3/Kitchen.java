package ru.ch.lab.lab3;

class Kitchen {
    //имитация сложной логики
    private static boolean isDishesClean = false;

    static void cook(int count) {
        if (!isDishesClean) {
            wash();
        }
        makeDough(count);
        bakeCrepes(count);
    }

    /**
     * Метод, который моет посуду)0)
     */
    private static void wash() {
        System.out.println("Моем посуду");
        isDishesClean = true;
    }

    private static void makeDough(int count) {
        System.out.println("Калькуляция теста для блинов вот тут:" +
                "\nhttps://www.timefry.ru/guide/kak_prigotovit_3_5_6_blinov/");
        System.out.println("Высчитали и сделали теста на " + count + " блинов");
    }

    private static void bakeCrepes(int count) {
        System.out.println("Отпекли блины " + count + " шт. и пошли курить");
    }
}

