package ru.ch.lab.lab1;

import ru.ch.lab.lab1.adapters.SquarePegAdapter;
import ru.ch.lab.lab1.round.RoundHole;
import ru.ch.lab.lab1.round.RoundPeg;
import ru.ch.lab.lab1.square.SquarePeg;

public class Demo {
    public static void main(String[] args) {
        RoundHole hole = new RoundHole(5);
        RoundPeg rpeg = new RoundPeg(5);

        System.out.println("round peg: " + hole.fits(rpeg));

        SquarePegAdapter smallSqPegAdapter = new SquarePegAdapter(new SquarePeg(2));
        SquarePegAdapter largeSqPegAdapter = new SquarePegAdapter(new SquarePeg(20));

        System.out.println("small square peg: " + hole.fits(smallSqPegAdapter));
        System.out.println("large square peg: " + hole.fits(largeSqPegAdapter));
    }
}