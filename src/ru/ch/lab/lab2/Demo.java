package ru.ch.lab.lab2;

import ru.ch.lab.lab2.products.IceCream;
import ru.ch.lab.lab2.products.interfaces.Product;
import ru.ch.lab.lab2.toppings.Sprinkle;
import ru.ch.lab.lab2.toppings.Syrup;

public class Demo {
    public static void main(String[] args) {
        Product iceCream = new IceCream("Мороженое", 80);
        Product syrup = new Syrup(iceCream);
        Product sprinkle = new Sprinkle(syrup);

        System.out.println("total: " + sprinkle.getPrice());
    }
}
