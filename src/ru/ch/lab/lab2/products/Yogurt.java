package ru.ch.lab.lab2.products;

import ru.ch.lab.lab2.products.interfaces.Product;

public class Yogurt implements Product {
    private String label;
    private double price;

    public Yogurt(String label, double price) {
        this.label = label;
        this.price = price;
    }

    public double getPrice() {
        return this.price;
    }

    public String getLabel() {
        return this.label;
    }
}
