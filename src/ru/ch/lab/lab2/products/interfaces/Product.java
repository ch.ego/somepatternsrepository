package ru.ch.lab.lab2.products.interfaces;

public interface Product {

    public double getPrice();

    public String getLabel();
}
