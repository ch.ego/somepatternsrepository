package ru.ch.lab.lab2.products;

public class Smoothie {
    private String label;
    private double price;

    public Smoothie(String label, double price) {
        this.label = label;
        this.price = price;
    }

    public double getPrice() {
        return this.price;
    }

    public String getLabel() {
        return this.label;
    }
}
