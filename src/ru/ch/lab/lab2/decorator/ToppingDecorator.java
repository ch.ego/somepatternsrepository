package ru.ch.lab.lab2.decorator;

import ru.ch.lab.lab2.products.interfaces.Product;

public class ToppingDecorator implements Product{
    private Product product;
    private String label;
    private double price;

    public ToppingDecorator(Product product, String label, double price) {
        this.product = product;
        this.label = label;
        this.price = price;
    }

    public double getPrice() {
        return this.price + product.getPrice();
    }

    public String getLabel() {
        return this.label + product.getLabel();
    }
}
