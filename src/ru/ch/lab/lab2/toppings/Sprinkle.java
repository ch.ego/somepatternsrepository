package ru.ch.lab.lab2.toppings;

import ru.ch.lab.lab2.decorator.ToppingDecorator;
import ru.ch.lab.lab2.products.interfaces.Product;

public class Sprinkle extends ToppingDecorator {
    public Sprinkle(Product product) {
        super(product, "Посыпка", 20);
    }
}
