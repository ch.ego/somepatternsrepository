package ru.ch.lab.lab2.toppings;

import ru.ch.lab.lab2.decorator.ToppingDecorator;
import ru.ch.lab.lab2.products.interfaces.Product;

public class Syrup extends ToppingDecorator {
    public Syrup(Product product) {
        super(product, "Сироп", 30);
    }
}
