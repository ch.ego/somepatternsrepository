package ru.ch.lab.lab4;

import ru.ch.lab.lab4.content.MyFile;
import ru.ch.lab.lab4.content.MyFolder;

public class Demo {
    public static void main(String[] args) {
        var file1 = new MyFile("1");
        var file2 = new MyFile("2");

        var folder1 = new MyFolder("1");
        var folder2 = new MyFolder("2");
        var root = new MyFolder("root");

        root.addContent(folder2);
        root.addContent(file2);
        folder2.addContent(file1);
        folder2.addContent(folder1);
        root.removeContent(file2);

        root.printContent();
    }
}
