package ru.ch.lab.lab4.content;

import ru.ch.lab.lab4.interfaces.Content;

import java.util.ArrayList;
import java.util.List;

public class MyFolder implements Content {
    String name;
    List<Content> contentList;

    public MyFolder(String name) {
        this.name = "FOLDER " + name;
        this.contentList = new ArrayList<>();
    }

    public void addContent(Content content) {
        this.contentList.add(content);
    }

    public void removeContent(Content content) {
        if (contains(content)) {
            this.contentList.remove(content);
        }
    }

    boolean contains(Content content) {
        return this.contentList.contains(content);
    }

    public void printContent() {
        System.out.println(this.name);
        this.contentList.forEach(e -> {
            System.out.println(e.getName());
        });
    }

    public String getName() {
        return name;
    }
}
