package ru.ch.lab.lab4.content;

import ru.ch.lab.lab4.interfaces.Content;

public class MyFile implements Content {
    String name;

    public MyFile(String name) {
        this.name = "FILE " + name;
    }

    @Override
    public String getName() {
        return name;
    }
}
