package ru.ch.lab.lab4.interfaces;

public interface Content {
    String getName();
}
